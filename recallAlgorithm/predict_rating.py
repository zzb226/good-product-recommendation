import happybase
import joblib

def predict_rating():

    # 加载已训练的模型
    svd = joblib.load('model.pkl')
    svd_item=joblib.load('model_item.pkl')

    # 连接到HBase数据库
    connection = happybase.Connection(host='hadoop1',autoconnect=False)
    connection.open()
    table_name = 'UserScore'
    table_name_item = 'item_score'
    table_name_out ='card_recall'
    table_name_item_out = 'product_recall'

    table = connection.table(table_name)  # 获取帖子打分表HBase表对象
    table_item = connection.table(table_name_item) # 获取商品打分表HBase表对象

    table_out = connection.table(table_name_out)  # 获取帖子召回输出HBase表对象
    table_item_out = connection.table(table_name_item_out) # 获取商品召回输出HBase表对象



    #帖子预测打分
    userId_set = set()
    itemId_set = set()
    score_out = {}

    for key, data in table.scan():  # 扫描表中的每一行数据
        userId = key.decode()  # HBase行键作为用户ID
        index = userId.find('_')  # 查找第一个 '_' 的索引位置
        if index != -1:  # 如果找到了 '_'
            userId = userId[:index]  # 截取从字符串开头到第一个 '_' 的部分
        else:
            userId = userId  # 如果没有找到 '_', 则将整个字符串作为结果
        userId_set.add(userId)
        # 获取列名信息
        columns = data.keys()
        # 遍历列名
        for column in columns:
            itemId_set.add(column.decode().replace('info:', ''))

    for userId in userId_set:
        for itemId in itemId_set:
            predicted_rating = svd.predict(userId, itemId)
            predicted_rating = format(predicted_rating, '.2f')
            score_out[itemId] = predicted_rating
        # 转换为字符串格式
        map_str = ", ".join([f"{key}:{value}" for key, value in score_out.items()])
        # 打印结果
        out_str="{" + map_str + "}"
        table_out.put(userId,{'info:itemList': out_str})
        print(f"用户 {userId} 对帖子 的预测评分：{out_str}")

        # 商品预测打分
        userId_set_item = set()
        itemId_set_item = set()
        score_out_item = {}

        for key, data in table_item.scan():  # 扫描表中的每一行数据
            userId = key.decode()  # HBase行键作为用户ID
            index = userId.find('_')  # 查找第一个 '_' 的索引位置
            if index != -1:  # 如果找到了 '_'
                userId = userId[:index]  # 截取从字符串开头到第一个 '_' 的部分
            else:
                userId = userId  # 如果没有找到 '_', 则将整个字符串作为结果
            userId_set_item.add(userId)
            # 获取列名信息
            columns = data.keys()
            # 遍历列名
            for column in columns:
                itemId_set_item.add(column.decode().replace('info:', ''))

        for userId in userId_set_item:
            for itemId in itemId_set_item:
                predicted_rating = svd_item.predict(userId, itemId)
                predicted_rating = format(predicted_rating, '.2f')
                score_out_item[itemId] = predicted_rating
            # 转换为字符串格式
            map_str = ", ".join([f"{key}:{value}" for key, value in score_out_item.items()])
            # 打印结果
            out_str = "{" + map_str + "}"
            table_item_out.put(userId, {'info:itemList': out_str})
            print(f"用户 {userId} 对物品 的预测评分：{out_str}")






