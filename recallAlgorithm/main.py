import schedule
import time
from predict_rating import predict_rating
from recallAlgorithm import recall_train

def job():
    print("执行")
    recall_train()
    predict_rating()


if __name__ == '__main__':
    # 定义定时任务，每5分钟执行一次
    schedule.every(10).minutes.do(job)

    while True:
        schedule.run_pending()
        time.sleep(1)





