import math
import random
import happybase

import joblib


class HBaseSVD():
    def __init__(self, table_name, connection, F=5, alpha=0.1, lmbda=0.1, max_iter=100):
        self.F = F                                 # 隐向量的维度
        self.P = dict()                           # 用户矩阵P，大小为[users_num, F]
        self.Q = dict()                           # 物品矩阵Q，大小为[item_nums, F]
        self.bu = dict()                          # 用户偏差系数
        self.bi = dict()                          # 物品偏差系数
        self.mu = 1.0                              # 全局偏差系数
        self.alpha = alpha                        # 学习率
        self.lmbda = lmbda                         # 正则项系数
        self.max_iter = max_iter                # 最大迭代次数
        self.table_name = table_name       # HBase表名
        self.connection = connection   # HBase连接对象
        self.rating_data = {}  # 初始化评分数据字典

    def load_data_from_hbase(self):
     table = self.connection.table(self.table_name)  # 获取HBase表对象
     for key, data in table.scan():  # 扫描表中的每一行数据
        user = key.decode()  # HBase行键作为用户ID
        index = user.find('_')  # 查找第一个 '_' 的索引位置
        if index != -1:  # 如果找到了 '_'
            user = user[:index]  # 截取从字符串开头到第一个 '_' 的部分
        else:
            user = user  # 如果没有找到 '_', 则将整个字符串作为结果
        self.rating_data.setdefault(user, {})  # 创建用户对应的评分字典
        for column, value in data.items():  # 遍历列族和列名
            item = column.decode()  # 列名作为物品ID
            item = item.replace('info:', '')
            rating = float(value.decode())  # 列值作为评分
            if user not in self.P:
                self.P[user] = [random.random() / math.sqrt(self.F) for _ in range(self.F)]  # 初始化用户矩阵P
                self.bu[user] = 0  # 初始化用户偏差系数
            if item not in self.Q:
                self.Q[item] = [random.random() / math.sqrt(self.F) for _ in range(self.F)]  # 初始化物品矩阵Q
                self.bi[item] = 0  # 初始化物品偏差系数
            self.rating_data[user][item] = rating  # 将评分存入训练数据字典


    def train(self):
        for step in range(self.max_iter):  # 迭代训练
            for user, items in self.rating_data.items():  # 遍历训练数据中的每个用户
                for item, rui in items.items():  # 遍历用户对物品的评分
                    rhat_ui = self.predict(user, item)  # 预测评分
                    e_ui = rui - rhat_ui  # 计算误差
                    self.bu[user] += self.alpha * (e_ui - self.lmbda * self.bu[user])  # 更新用户偏差系数
                    self.bi[item] += self.alpha * (e_ui - self.lmbda * self.bi[item])  # 更新物品偏差系数
                    for k in range(self.F):
                        self.P[user][k] += self.alpha * (e_ui * self.Q[item][k] - self.lmbda * self.P[user][k])  # 更新用户矩阵P
                        self.Q[item][k] += self.alpha * (e_ui * self.P[user][k] - self.lmbda * self.Q[item][k])  # 更新物品矩阵Q
            self.alpha *= 0.1  # 逐步缩小学习率

    def predict(self, user, item):
        return sum(self.P[user][f] * self.Q[item][f] for f in range(self.F)) + self.bu[user] + self.bi[item] + self.mu





def recall_train():
    # 连接到HBase数据库
    connection = happybase.Connection(host='hadoop1',autoconnect=False)
    connection.open()
    table_name = 'UserScore'
    table_name_item = 'item_score'

    # 创建HBaseSVD实例
    svd = HBaseSVD(table_name, connection)
    svd_item = HBaseSVD(table_name_item, connection)

    # 从HBase数据库加载训练数据
    svd.load_data_from_hbase()
    svd_item.load_data_from_hbase()

    # 训练模型
    svd.train()
    svd_item.train()

    # 清除不可序列化的属性
    svd.connection = None
    svd_item.connection=None

    # 保存模型
    joblib.dump(svd, 'model.pkl')
    joblib.dump(svd_item,'model_item.pkl')

