package com.banma.utils;
 
import com.alibaba.fastjson.JSON;
import lombok.extern.java.Log;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 对象工具类
 *
 */
public class ObjectUtil {
 
    /**
     * 判断对象中部分属性值是否不为空
     *
     * @param object       对象
     * @return true:不为空 false:为空
     */
    public static boolean checkObjFieldsIsNotNull(Object object) {
        // 如果对象为空，返回false
        if (null == object) {
            return false;
        }
 
        // 获取对象的所有属性
        Field[] fields = object.getClass().getDeclaredFields();
        // 遍历属性
        for (Field field : fields) {
            String fieldName = field.getName();
            // 获取属性的get方法
            String methodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            //调用get方法
            try {
                Method method = object.getClass().getMethod(methodName);
                Object value = method.invoke(object);
                if (null == value ||"".equals(value)) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public static boolean isJSON() {
        return isJSON(null);
    }
    /**
     * 判断字符串是否是JSON格式
     *
     * @param eventData 事件数据
     * @return true:是 false:不是
     */
    public static boolean isJSON(String eventData) {
        boolean result = false;
        if (null != eventData) {
            try {
                JSON.parseObject(eventData);
                result = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}