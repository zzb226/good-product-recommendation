package com.banma.domain.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 请求类
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataRequest {
    /**
     * 客户端唯一编号
     */
    private String appId;
    /**
     * 加密后业务相关的入参
     */
    private String data;
}
