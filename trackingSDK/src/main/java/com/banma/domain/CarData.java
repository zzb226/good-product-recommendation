package com.banma.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 汽车数据类
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CarData {
    /**
     * 加速度
     */
    private double acceleration;
    /**
     * 经度
     */
    private double longitude;
    /**
     * 纬度
     */
    private double latitude;
    /**
     * 速度
     */
    private double velocity;
}
