package com.banma.client;

import com.alibaba.fastjson.JSON;
import com.banma.common.DataException;
import com.banma.common.ReturnCodeEnum;
import com.banma.domain.TrackingData;
import com.banma.domain.common.ApiRequest;
import com.banma.domain.common.DataRequest;

import com.banma.domain.common.DataResponse;
import com.banma.utils.HttpUtil;
import com.banma.utils.RsaUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 客户端抽象类
 *
 */

@Slf4j
abstract class ClientAbstract {
    public static String post(ApiRequest<TrackingData> request) {
        // 1.封装请求参数
        DataRequest dataRequest = DataRequest.builder()
                .appId(request.getAppId())
                .data(RsaUtil.encryptPublic(request.getPublicKey(), JSON.toJSONString(request.getData())))
                .build();
        // 2.发送请求
        String s = HttpUtil.doPost(request.getUrl(), JSON.toJSONString(dataRequest));
        // 3.判断响应
        if (StringUtils.isBlank(s)) {
            log.error("client post api result is null!");
            throw new DataException(ReturnCodeEnum.API_ERROR);
        }
        // 4.解析响应
        DataResponse hopeResponse = JSON.parseObject(s, DataResponse.class);
        if (!hopeResponse.isSuccess()) {
            log.error("client post api error! hopeResponse={}", hopeResponse);
            throw new DataException(ReturnCodeEnum.API_ERROR.getCode(), hopeResponse.getMessage());
        }
        // 5.返回结果
        return RsaUtil.decryptPublic(request.getPublicKey(), hopeResponse.getData());
    }
}
