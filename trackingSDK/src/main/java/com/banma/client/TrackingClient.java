package com.banma.client;

import com.alibaba.fastjson.JSON;
import com.banma.domain.CarData;
import com.banma.domain.TrackingData;
import com.banma.domain.common.ApiRequest;
import com.banma.domain.common.DataResponse;
import com.banma.utils.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 数据客户端
 *
 */
@Slf4j
public class TrackingClient extends ClientAbstract {
    public static DataResponse sendTrackingData(ApiRequest<TrackingData> request) {
        try {
            // 1.校验参数
            TrackingData data = request.getData();
            //判断对象中属性值是否不空
            if(!ObjectUtil.checkObjFieldsIsNotNull(data)){
                log.error("TrackingClient sendTrackingData is exception! request={}", request);
                return DataResponse.builder().success(false).message("some fields are null!").build();
            }
          //  CarData carData = data.getCarData();
            //判断车辆数据是否为空
//            if(!ObjectUtil.checkObjFieldsIsNotNull(carData)){
//                log.error("TrackingClient sendTrackingData is exception! request={}", request);
//                return DataResponse.builder().success(false).message("some fields are null!").build();
//            }
            //判断事件数据是否是JSON格式
            if(!ObjectUtil.isJSON(data.getEventData())){
                log.error("TrackingClient sendTrackingData is exception! request={}", request);
                return DataResponse.builder().success(false).message("eventData is not JSON format!").build();
            }
            // 2.发送请求
            String str = post(request);
            return JSON.parseObject(str,DataResponse.class);
        } catch (Exception e) {
            log.error("SysUserClient queryUserList is exception! request={}", request);
            return DataResponse.builder().success(false).message(e.getMessage()).build();
        }
    }

}
