package com.banma;

import com.banma.client.TrackingClient;
import com.banma.domain.TrackingData;
import com.banma.domain.common.ApiRequest;
import com.banma.domain.common.DataResponse;

/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 主类
 *
 */
public class Main {
    // SDK方法封装
    public static DataResponse sendTrackingData(TrackingData trackingData, String url, String appId, String publicKey){
        ApiRequest<TrackingData> request = ApiRequest.<TrackingData>builder()
                .url(url)
                .publicKey(publicKey)
                .appId(appId)
                .data(trackingData)
                .build();
        return TrackingClient.sendTrackingData(request);
    }
}
