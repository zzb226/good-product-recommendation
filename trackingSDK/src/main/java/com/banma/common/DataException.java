package com.banma.common;
/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 异常类
 *
 */

public class DataException extends RuntimeException{
    private final String code;
    private final String message;

    public DataException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public DataException(ReturnCodeEnum apiError) {
        this.code = apiError.getCode();
        this.message = apiError.getMessage();
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
