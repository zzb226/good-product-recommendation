package com.banma.common;
/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 返回码枚举类
 *
 */
public enum ReturnCodeEnum {
    //API调用错误
    API_ERROR("API_ERROR", "API调用异常");
    private String code;
    private String message;
    ReturnCodeEnum(String code, String message) {
    }
    public String getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
