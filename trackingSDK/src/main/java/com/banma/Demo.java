package com.banma;


import com.banma.client.TrackingClient;
import com.banma.domain.CarData;
import com.banma.domain.TrackingData;
import com.banma.domain.common.ApiRequest;
import com.banma.domain.common.DataResponse;

import java.util.Date;

/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 调用示例
 *
 */
public class Demo {

    /**
     * 公钥
     */
    private static final String PUBLIC_KEY_STRING =
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDNJ4m9j3T6gRAKjdq3iZ7ID7Lttu0oTE7J2Oy67ivmdbzA0Te7PnPsY8MW1lTkJLbF73wzOolQPG/0tqFfjs5DqD1Pe/Sf7W1f04XAdrcBK+lcGM3VPhIdlnJrpWxtAl90RmpS9xKmd/pPYSPw8GzUTqbEnjeeG4TTkdpbdFESKQIDAQAB";
    
    public static void main(String[] args) {

        TrackingData dto = new TrackingData("007","V20230206","v1.0","1","account","{\"h\":\"l\"}","1111",new Date(),new Date(),new Date(),"007","1111");
        ApiRequest<TrackingData> request = ApiRequest.<TrackingData>builder()
                .appId("000001")
                .publicKey(PUBLIC_KEY_STRING)
                .url("http://localhost:84/tracking/client/lb")
                .data(dto)
                .build();
        DataResponse response = TrackingClient.sendTrackingData(request);
        System.out.println(response);
    }

}
