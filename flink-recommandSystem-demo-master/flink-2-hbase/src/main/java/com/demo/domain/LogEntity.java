package com.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author XINZE
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogEntity {

    private String  userId;

    private List<String> itemId;

    private Integer type;

    private String triggerTime;

    private Integer isRecommend;

    private Integer event_type;




}
