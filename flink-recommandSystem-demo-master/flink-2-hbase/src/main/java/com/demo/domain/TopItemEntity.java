package com.demo.domain;

/**
 * @author XINZE
 */
public class TopItemEntity {

    private String itemId;
    private int actionTimes;
    private String rankName;

    public TopItemEntity(String itemId, int count) {
        this.itemId = itemId;
        this.actionTimes = count;
        rankName = "1";
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getActionTimes() {
        return actionTimes;
    }

    public void setActionTimes(int actionTimes) {
        this.actionTimes = actionTimes;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    @Override
    public String toString() {
        return "TopItemEntity{" +
                "itemId='" + itemId + '\'' +
                ", actionTimes=" + actionTimes +
                ", rankName='" + rankName + '\'' +
                '}';
    }
}
