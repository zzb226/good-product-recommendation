package com.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BehaviorData {

    private String userId;

    private String itemId;

    private Integer type;

    private Integer isClick = 0;

    private Integer isLike = 0;

    private Integer isComment = 0;

    private Integer noLike = 0;

    private Integer noLikeAuthor = 0;

    private Integer isBuy =0;

    private Integer isCollect =0;

    private Date triggerTime;

}
