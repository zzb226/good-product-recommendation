package com.demo.agg;

import com.demo.domain.LogEntity;
import com.demo.domain.TopItemEntity;
import org.apache.flink.api.common.functions.AggregateFunction;

public class CountAgg implements AggregateFunction<LogEntity, Integer, TopItemEntity> {

    private String itemId;

    @Override
    public Integer createAccumulator() {
        return 0;
    }

    @Override
    public Integer add(LogEntity logEntity, Integer integer) {
        itemId = logEntity.getItemId().get(0);
        return integer + 1;
    }

    @Override
    public TopItemEntity getResult(Integer integer) {
        return new TopItemEntity(itemId, integer);
    }

    @Override
    public Integer merge(Integer integer, Integer acc1) {
        return integer + acc1;
    }
}
