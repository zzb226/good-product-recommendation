package com.demo.sink;

import com.demo.domain.LogEntity;
import com.demo.util.Property;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.UUID;

public class HbaseSinkFunction extends RichSinkFunction<LogEntity> {

    private final static Logger logger = LoggerFactory.getLogger(HbaseSinkFunction.class);
    private static BufferedMutator mutator;
    private static String Tablename="behavior_log";
    public static Connection conn;

    @Override
    public void open(Configuration parameters) throws Exception {
        org.apache.hadoop.conf.Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.rootdir", Property.getStrValue("hbase.rootdir"));
        conf.set("hbase.zookeeper.quorum", Property.getStrValue("hbase.zookeeper.quorum"));
        conf.set("hbase.client.scanner.timeout.period", Property.getStrValue("hbase.client.scanner.timeout.period"));
        conf.set("hbase.rpc.timeout", Property.getStrValue("hbase.rpc.timeout"));
        try {
            conn = ConnectionFactory.createConnection(conf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedMutatorParams params = new BufferedMutatorParams(TableName.valueOf(Tablename));

        //缓存大小
        params.writeBufferSize(2*1024*1024);

        try {
            mutator = conn.getBufferedMutator(params);
        }catch (IOException e){
            logger.error("当前获取bufferedMutator 失败：" + e.getMessage());
        }
    }

    @Override
    public void close() throws Exception {
        if (mutator != null) {
            mutator.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    @Override
    public void invoke(LogEntity value, Context context) throws Exception {
        String familyName = "info";
        Put put = new Put(Bytes.toBytes(UUID.randomUUID().toString()));
        put.addColumn(Bytes.toBytes(familyName),Bytes.toBytes("userId"),Bytes.toBytes(value.getUserId()));
        put.addColumn(Bytes.toBytes(familyName),Bytes.toBytes("itemId"),Bytes.toBytes(value.getItemId().toString()));
        put.addColumn(Bytes.toBytes(familyName),Bytes.toBytes("type"),Bytes.toBytes(value.getType().toString()));
        put.addColumn(Bytes.toBytes(familyName),Bytes.toBytes("triggerTime"),Bytes.toBytes(value.getTriggerTime()));
        put.addColumn(Bytes.toBytes(familyName),Bytes.toBytes("isRecommend"),Bytes.toBytes(value.getIsRecommend().toString()));
        put.addColumn(Bytes.toBytes(familyName),Bytes.toBytes("event_type"),Bytes.toBytes(value.getEvent_type().toString()));
        mutator.mutate(put);
        // 指定时间内的数据强制刷写到hbase
        mutator.flush();
    }
}
