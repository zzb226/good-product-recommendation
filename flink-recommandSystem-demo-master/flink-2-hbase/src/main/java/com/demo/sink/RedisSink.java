package com.demo.sink;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import redis.clients.jedis.Jedis;

import java.util.List;

public class RedisSink extends RichSinkFunction<List<String>>{
        private Jedis jedis = null;
        private String host;
        private int port; // 默认端口6379
        private String key;
        //连接地址、端口号、redis key值
        public RedisSink(String host, int port, String key) {
            this.host = host;
            this.port = port;
            this.key = key;
        }
        @Override
        public void open(Configuration parameters) throws Exception {
            jedis = new Jedis(host, port);
        }
        @Override
        public void invoke(List<String> value, Context context) {
            jedis.del(key);
            for(String item : value) {
                jedis.rpush(key, item);
            }
        }
        @Override
        public void close() throws Exception {
            super.close();
            jedis.close();
        }
}
