package com.demo.sink;


import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MyHbaseSink extends RichSinkFunction<Map<String, Map<String, Integer>>> {
    private Connection conn;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);

        //创建一个Hbase的连接
        //创建hbase配置对象
        org.apache.hadoop.conf.Configuration conf= HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "hadoop1,hadoop2,hadoop3");// zookeeper地址
        conf.set("hbase.zookeeper.property.clientPort", "2181");// zookeeper端口
        //通过配置对象创建连接
        conn = ConnectionFactory.createConnection(conf);
    }

    @Override
    public void close() throws Exception {
        super.close();
        conn.close();
    }

    @Override
    public void invoke(Map<String, Map<String, Integer>> value, Context context) throws Exception {
        //获取table对象
        Table table = conn.getTable(TableName.valueOf("UserScore"));
        //创建put对象，对应put命令，封装rowkey、列族、列名、列值
        Set<String> keys = value.keySet();
        // 创建put对象，对应put命令，封装rowkey、列族、列名、列值
        List<Put> puts = new ArrayList<>();
        for (String key : keys) {
            for(String k2 :value.get(key).keySet()){
                System.out.println("帖子"+key+"_"+k2+"_"+String.valueOf(value.get(key).get(k2)));
                Put put = new Put(Bytes.toBytes(key+"_"+k2));
                put.addColumn(Bytes.toBytes("info"),Bytes.toBytes(k2),Bytes.toBytes(String.valueOf(value.get(key).get(k2))));
                puts.add(put);
            }
        }
        // 执行批量put命令
        table.put(puts);
        //关闭table
        table.close();
    }
}
