package com.demo.sink;

import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

public class CardHbaseSink extends RichSinkFunction<Tuple3<String,String,Integer>> {
    private Connection conn;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);

        //创建一个Hbase的连接
        //创建hbase配置对象
        org.apache.hadoop.conf.Configuration conf= HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "hadoop1,hadoop2,hadoop3");// zookeeper地址
        conf.set("hbase.zookeeper.property.clientPort", "2181");// zookeeper端口
        //通过配置对象创建连接
        conn = ConnectionFactory.createConnection(conf);
    }

    @Override
    public void close() throws Exception {
        super.close();
        conn.close();
    }

    @Override
    public void invoke(Tuple3<String,String,Integer> value, Context context) throws Exception {
        //获取table对象
        Table table = conn.getTable(TableName.valueOf("UserScore"));
        // 创建put对象，对应put命令，封装rowkey、列族、列名、列值
        System.out.println("帖子"+value.f0+"_"+value.f1+"_"+String.valueOf(value.f2));
        Put put = new Put(Bytes.toBytes(value.f0+"_"+value.f1));
        put.addColumn(Bytes.toBytes("info"),Bytes.toBytes(value.f1),Bytes.toBytes(String.valueOf(value.f2)));
        table.put(put);
        //关闭table
        table.close();
    }
}
