package com.demo.task;

import com.demo.agg.CountAgg;
import com.demo.domain.LogEntity;
import com.demo.domain.TopItemEntity;
import com.demo.util.Property;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.util.serialization.JSONKeyValueDeserializationSchema;
import org.apache.flink.util.Collector;
import redis.clients.jedis.Jedis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TopNCardsTask {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        Properties properties = Property.getKafkaProperties("topNCards");
        FlinkKafkaConsumer<ObjectNode> fkc = new FlinkKafkaConsumer("track", new JSONKeyValueDeserializationSchema(true), properties);
        DataStreamSource<ObjectNode> dataStream = env.addSource(fkc);

        SingleOutputStreamOperator<List<String>> soso =
        dataStream.map(new TopNMapFunction())
                .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<LogEntity>(Time.milliseconds(10000)) {
                    @Override
                    public long extractTimestamp(LogEntity element) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date datatime = new Date();
                        try {
                            datatime = sdf.parse(element.getTriggerTime());
                        } catch (ParseException e) {
                            throw new RuntimeException(e);
                        }
                        return datatime.getTime();
                    }
                })
                .filter(value -> value.getType() == 0 && value.getEvent_type() == 2)
                .keyBy(value -> value.getItemId().get(0))
                .window(SlidingEventTimeWindows.of(Time.hours(1), Time.hours(1)))
//                .window(SlidingEventTimeWindows.of(Time.hours(1), Time.minutes(10)))
                //.window(SlidingEventTimeWindows.of(Time.hours(1), Time.hours(1)))
                .aggregate(new CountAgg(), new WindowResultFunction())
                .keyBy("windowEnd")
                .process(new TopNHotItems(50));

        soso.print();
//        soso.addSink(new RedisSink<>(conf, new TopNRedisSink()));
        soso.addSink(new SinkHashRedis(Property.getStrValue("redis.host"),Property.getIntValue("redis.port"), null));

        env.execute("Top N Cards ");
    }

    public static class TopNMapFunction implements MapFunction<ObjectNode, LogEntity> {
        @Override
        public LogEntity map(ObjectNode jsonNode) throws Exception {
            String userId = jsonNode.get("value").get("userId").asText();
            ArrayNode arrayNode = (ArrayNode) jsonNode.get("value").get("itemId");
            List<String> itemList = new ArrayList<>();
            for (JsonNode node : arrayNode) {
                String itemId = node.asText();
                itemList.add(itemId);
            }
            int type = jsonNode.get("value").get("type").asInt();
            String triggerTime = jsonNode.get("value").get("triggerTime").asText();
            int isRecommend = jsonNode.get("value").get("isRecommend").asInt();
            int event_type = jsonNode.get("value").get("event_type").asInt();
            LogEntity logEntity = new LogEntity(userId, itemList, type, triggerTime, isRecommend, event_type);
            return logEntity;
        }
    }

    public static class SinkHashRedis extends RichSinkFunction<List<String>> {

        private Jedis jedis = null;

        private String host;

        private int port; // 默认端口6379

        private String key;

        //连接地址、端口号、redis key值
        public SinkHashRedis(String host, int port, String key) {
            this.host = host;
            this.port = port;
            this.key = key;
        }

        @Override
        public void open(Configuration parameters) throws Exception {
            jedis = new Jedis(host, port);
        }

        @Override
        public void invoke(List<String> value, Context context) {
            jedis.del("top_card");
            for(String item : value) {
                jedis.rpush("top_card", item);
            }
        }

        @Override
        public void close() throws Exception {
            super.close();
            jedis.close();
        }
    }

    private static class WindowResultFunction implements WindowFunction<TopItemEntity, UserViewCount, String, TimeWindow> {


        @Override
        public void apply(String key, TimeWindow window, Iterable<TopItemEntity> input, Collector<UserViewCount> out) throws Exception {
            TopItemEntity count = input.iterator().next();
            out.collect(new UserViewCount(key, window.getEnd(), count.getActionTimes()));
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    public static  class UserViewCount {
        private String itemId;
        private long windowEnd;
        private long viewCount;

    }

    private static class TopNHotItems extends KeyedProcessFunction<Tuple, UserViewCount, List<String>> {

        private int topSize;
        private ListState<UserViewCount> userViewCountListState;

        public TopNHotItems(int topSize) {
            this.topSize = topSize;
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<List<String>> out) throws Exception {
            super.onTimer(timestamp, ctx, out);
            List<UserViewCount> userViewCounts = new ArrayList<>();
            for(UserViewCount userViewCount : userViewCountListState.get()) {
                userViewCounts.add(userViewCount);
            }

            userViewCountListState.clear();

            userViewCounts.sort(new Comparator<UserViewCount>() {
                @Override
                public int compare(UserViewCount o1, UserViewCount o2) {
                    return (int)(o2.viewCount - o1.viewCount);
                }
            });
            List<String> topList = new ArrayList<String>();
            int i = 0;
            for(UserViewCount item : userViewCounts){
                i++;
                topList.add(item.getItemId());
                if(i>=topSize){
                    break;
                }
            }
            int j = 0;
            while(topList.size()<topSize){
                j++;
                if(!topList.contains(String.valueOf(j))){
                    topList.add(String.valueOf(j));
                }
            }

            out.collect(topList);
        }

        @Override
        public void open(org.apache.flink.configuration.Configuration parameters) throws Exception {
            super.open(parameters);
            ListStateDescriptor<UserViewCount> userViewCountListStateDescriptor = new ListStateDescriptor<>(
                    "user-state",
                    UserViewCount.class
            );
            userViewCountListState = getRuntimeContext().getListState(userViewCountListStateDescriptor);
        }

        @Override
        public void processElement(UserViewCount value, Context ctx, Collector<List<String>> out) throws Exception {
            userViewCountListState.add(value);
            ctx.timerService().registerEventTimeTimer(value.windowEnd + 1000);
        }
    }


}

