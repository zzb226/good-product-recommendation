package com.demo.task;

import com.demo.domain.BehaviorData;
import com.demo.domain.LogEntity;
import com.demo.sink.CardHbaseSink;
import com.demo.sink.ItemHbaseSink;
import com.demo.sink.ItemHbaseSink2;
import com.demo.sink.MyHbaseSink;
import com.demo.util.Property;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.util.serialization.JSONKeyValueDeserializationSchema;
import org.apache.flink.util.Collector;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

public class DealLogTask {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        Properties properties = Property.getKafkaProperties("consumer-group");
        FlinkKafkaConsumer<ObjectNode> fkc = new FlinkKafkaConsumer("track", new JSONKeyValueDeserializationSchema(true), properties);
        DataStreamSource<ObjectNode> dataStream = env.addSource(fkc);

        SingleOutputStreamOperator<BehaviorData> behaviorDataStreamOperator = dataStream.flatMap(new FlatMapFun());

        SingleOutputStreamOperator<BehaviorData> logsoso = behaviorDataStreamOperator.
                // 抽取时间戳做watermark 以 秒 为单位
                        assignTimestampsAndWatermarks(WatermarkStrategy
                        //创建水位线对象，并指定延迟指定时间。该方法用于处理乱序时间
                        .<BehaviorData>forBoundedOutOfOrderness(Duration.ofSeconds(5))    //延迟时间
                        //指定 数据 中的时间戳
                        .withTimestampAssigner(new SerializableTimestampAssigner<BehaviorData>() {
                            @Override
                            public long extractTimestamp(BehaviorData element, long l) {
                                return element.getTriggerTime().getTime();
                            }
                        }));

        //处理帖子
        //ingleOutputStreamOperator<Map<String, Map<String, Integer>>> soso =
                logsoso.filter(value -> value.getType() == 0) //帖子
                .keyBy(value -> value.getUserId() + '_' + value.getItemId())
                        .window(SlidingEventTimeWindows.of(Time.hours(1), Time.minutes(10)))
                //.window(SlidingEventTimeWindows.of(Time.seconds(20), Time.seconds(10)))
                .process(new ProcessWindowFunction<BehaviorData, Tuple3<String, String, Integer>, String, TimeWindow>() {
                    @Override
                    public void process(String key, Context context, Iterable<BehaviorData> iterable, Collector<Tuple3<String, String, Integer>> out) throws Exception {
                        Iterator<BehaviorData> it = iterable.iterator();

                        int score = 0;
                        while (it.hasNext()) {
                            BehaviorData a = it.next();
                            score = score + a.getIsClick() + (a.getIsLike() + a.getIsComment() + a.getNoLike() + a.getNoLikeAuthor()) * 2;
                        }
                        if (score > 10) {
                            score = 10;
                        } else if (score < -10) {
                            score = -10;
                        }
                        String[] split = key.split("_");
                        out.collect(new Tuple3<String, String, Integer>(split[0], split[1], score));
                    }
                }).addSink(new CardHbaseSink());
                //.global()
                //.keyBy(value -> value.f0)
                //.process(new ScoreAggregator());



        //处理商品
       // SingleOutputStreamOperator<Map<String, Map<String, Integer>>> item_soso =
                logsoso.filter(value -> value.getType() == 1) //商品
                        .keyBy(value -> value.getUserId() + '_' + value.getItemId())
                        .window(SlidingEventTimeWindows.of(Time.hours(1), Time.minutes(10)))
                        //.window(SlidingEventTimeWindows.of(Time.seconds(20), Time.seconds(10)))
                        .process(new ProcessWindowFunction<BehaviorData, Tuple3<String, String, Integer>, String, TimeWindow>() {
                            @Override
                            public void process(String key, Context context, Iterable<BehaviorData> iterable, Collector<Tuple3<String, String, Integer>> out) throws Exception {
                                Iterator<BehaviorData> it = iterable.iterator();

                                int score = 0;
                                while (it.hasNext()) {
                                    BehaviorData a = it.next();
                                    score = score + a.getIsClick() + a.getIsBuy()*3 + a.getIsCollect()*2;
                                }
                                if (score > 10) {
                                    score = 10;
                                } else if (score < -10) {
                                    score = -10;
                                }
                                String[] split = key.split("_");
                                out.collect(new Tuple3<String, String, Integer>(split[0], split[1], score));
                            }
                        }).addSink(new ItemHbaseSink2());
                        //.global()
                        //.keyBy(value -> value.f0)
                        //.process(new ScoreAggregator());


        //soso.addSink(new MyHbaseSink());
        //item_soso.addSink(new ItemHbaseSink());








        env.execute("Log message receive");
    }


    public static class FlatMapFun implements FlatMapFunction<ObjectNode, BehaviorData> {

        @Override
        public void flatMap(ObjectNode value, Collector<BehaviorData> out) throws Exception {
            String userId = value.get("value").get("userId").asText();
            ArrayNode arrayNode = (ArrayNode) value.get("value").get("itemId");
            List<String> itemList = new ArrayList<>();
            for (JsonNode node : arrayNode) {
                String itemId = node.asText();
                itemList.add(itemId);
            }
            int type = value.get("value").get("type").asInt();
            String triggerTime = value.get("value").get("triggerTime").asText();
            int isRecommend = value.get("value").get("isRecommend").asInt();
            int event_type = value.get("value").get("eventType").asInt();
            LogEntity logEntity = new LogEntity(userId,itemList,type,triggerTime,isRecommend,event_type);
               if(event_type==0){
                   //点击
                   for(String item:itemList){
                       BehaviorData behaviorData = initBehaviorData(userId, item, type,triggerTime);
                       behaviorData.setIsClick(1);
                       out.collect(behaviorData);
                   }
               }else if(event_type==1){
                   //浏览未查看
                   for(String item:itemList){
                       BehaviorData behaviorData = initBehaviorData(userId, item, type,triggerTime);
                       behaviorData.setIsClick(-1);
                       out.collect(behaviorData);
                   }
               }else if(event_type==2){
                   //点赞
                   for(String item:itemList){
                       BehaviorData behaviorData = initBehaviorData(userId, item, type,triggerTime);
                       behaviorData.setIsLike(1);
                       out.collect(behaviorData);
                   }
               }else if(event_type==3){
                   //不喜欢作者
                   for(String item:itemList){
                       BehaviorData behaviorData = initBehaviorData(userId, item, type,triggerTime);
                       behaviorData.setNoLikeAuthor(-1);
                       out.collect(behaviorData);
                   }
               }else if(event_type==4){
                   //不喜欢帖子
                   for(String item:itemList){
                       BehaviorData behaviorData = initBehaviorData(userId, item, type,triggerTime);
                       behaviorData.setNoLike(-1);
                       out.collect(behaviorData);
                   }
               }else if(event_type==5){
                   //评论帖子
                   for(String item:itemList){
                       BehaviorData behaviorData = initBehaviorData(userId, item, type,triggerTime);
                       behaviorData.setIsComment(1);
                       out.collect(behaviorData);
                   }
               }else if(event_type==6){
                   //购买
                   for(String item:itemList){
                       BehaviorData behaviorData = initBehaviorData(userId, item, type,triggerTime);
                       behaviorData.setIsBuy(1);
                       out.collect(behaviorData);
                   }
               }else if(event_type==7){
                   //收藏
                   for(String item:itemList){
                       BehaviorData behaviorData = initBehaviorData(userId, item, type,triggerTime);
                       behaviorData.setIsCollect(1);
                       out.collect(behaviorData);
                   }
               }

        }

        private BehaviorData initBehaviorData(String userId,String item,Integer type,String triggerTime){
            BehaviorData behaviorData = new BehaviorData();
            try {
                behaviorData.setUserId(userId);
                behaviorData.setItemId(item);
                behaviorData.setType(type);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                behaviorData.setTriggerTime(simpleDateFormat.parse(triggerTime));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return behaviorData;
        }
    }


}

