package com.demo.task;

import com.demo.domain.LogEntity;
import com.demo.sink.HbaseSinkFunction;
import com.demo.util.Property;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.util.serialization.JSONKeyValueDeserializationSchema;

import java.util.*;

public class BehavierTask {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        Properties properties = Property.getKafkaProperties("log");
        FlinkKafkaConsumer<ObjectNode> fkc = new FlinkKafkaConsumer("track", new JSONKeyValueDeserializationSchema(true), properties);
        DataStreamSource<ObjectNode> dataStream = env.addSource(fkc);

        dataStream.map( new LogMapFunction())
                .addSink(new HbaseSinkFunction());

        env.execute("Behavier Log");
    }

    public static class LogMapFunction implements MapFunction<ObjectNode, LogEntity> {
        @Override
        public LogEntity map(ObjectNode jsonNode) throws Exception {
            String userId = jsonNode.get("value").get("userId").asText();
            ArrayNode arrayNode = (ArrayNode) jsonNode.get("value").get("itemId");
            List<String> itemList = new ArrayList<>();
            for (JsonNode node : arrayNode) {
                String itemId = node.asText();
                itemList.add(itemId);
            }
            int type = jsonNode.get("value").get("type").asInt();
            String triggerTime = jsonNode.get("value").get("triggerTime").asText();
            int isRecommend = jsonNode.get("value").get("isRecommend").asInt();
            int event_type = jsonNode.get("value").get("event_type").asInt();
            LogEntity logEntity = new LogEntity(userId, itemList, type, triggerTime, isRecommend, event_type);
            return logEntity;
        }
    }
}

