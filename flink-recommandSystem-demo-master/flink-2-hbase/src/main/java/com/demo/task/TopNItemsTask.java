package com.demo.task;

import com.demo.agg.CountAgg;
import com.demo.domain.LogEntity;
import com.demo.domain.TopItemEntity;
import com.demo.domain.UserViewCount;
import com.demo.sink.HbaseSinkFunction;
import com.demo.sink.RedisSink;
import com.demo.util.Property;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.util.serialization.JSONKeyValueDeserializationSchema;
import org.apache.flink.util.Collector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TopNItemsTask {
    public static void main(String[] args) throws Exception {
        // 创建kafka连接
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        Properties properties = Property.getKafkaProperties("TopN_log");
        FlinkKafkaConsumer<ObjectNode> fkc = new FlinkKafkaConsumer("track", new JSONKeyValueDeserializationSchema(true), properties);
        DataStreamSource<ObjectNode> dataStream = env.addSource(fkc);

        // 行为日志存储
        dataStream.map( new LogMapFunction())
                .addSink(new HbaseSinkFunction());

        // TopN 商品处理任务
        SingleOutputStreamOperator<List<String>> sosoProd = dataStream.map(new LogMapFunction())
                .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<LogEntity>(Time.milliseconds(10000)) {
                    @Override
                    public long extractTimestamp(LogEntity element) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date datatime = new Date();
                        try {
                            datatime = sdf.parse(element.getTriggerTime());
                        } catch (ParseException e) {
                            throw new RuntimeException(e);
                        }
                        return datatime.getTime();
                    }
                })
                .filter(value -> value.getType() == 1 && value.getEvent_type() == 6)
                .keyBy(value -> value.getItemId().get(0))
                .window(SlidingEventTimeWindows.of(Time.hours(1), Time.minutes(10)))
                //.window(SlidingEventTimeWindows.of(Time.hours(5), Time.hours(5)))
                .aggregate(new CountAgg(), new WindowResultFunction())
                .keyBy("windowEnd")
                .process(new TopNProds(50));

        sosoProd.print();
        sosoProd.addSink(new RedisSink(Property.getStrValue("redis.host"),Property.getIntValue("redis.port"), "top_prod"));

        SingleOutputStreamOperator<List<String>> sosoCard =
                dataStream.map(new LogMapFunction())
                        .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<LogEntity>(Time.milliseconds(10000)) {
                            @Override
                            public long extractTimestamp(LogEntity element) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date datatime = new Date();
                                try {
                                    datatime = sdf.parse(element.getTriggerTime());
                                } catch (ParseException e) {
                                    throw new RuntimeException(e);
                                }
                                return datatime.getTime();
                            }
                        })
                        .filter(value -> value.getType() == 0 && value.getEvent_type() == 2)
                        .keyBy(value -> value.getItemId().get(0))
                        .window(SlidingEventTimeWindows.of(Time.hours(1), Time.minutes(10)))
                        //.window(SlidingEventTimeWindows.of(Time.hours(1), Time.hours(1)))
                        .aggregate(new CountAgg(), new WindowResultFunction())
                        .keyBy("windowEnd")
                        .process(new TopNCards(50));

        sosoCard.print();
        sosoCard.addSink(new RedisSink(Property.getStrValue("redis.host"),Property.getIntValue("redis.port"), "top_card"));

        env.execute("TopN_log");
    }

    /**
     * 自定义Map方法，处理Kafka传来的Json
     */
    public static class LogMapFunction implements MapFunction<ObjectNode, LogEntity> {
        @Override
        public LogEntity map(ObjectNode jsonNode) throws Exception {
            String userId = jsonNode.get("value").get("userId").asText();
            ArrayNode arrayNode = (ArrayNode) jsonNode.get("value").get("itemId");
            List<String> itemList = new ArrayList<>();
            for (JsonNode node : arrayNode) {
                String itemId = node.asText();
                itemList.add(itemId);
            }
            int type = jsonNode.get("value").get("type").asInt();
            String triggerTime = jsonNode.get("value").get("triggerTime").asText();
            int isRecommend = jsonNode.get("value").get("isRecommend").asInt();
            int event_type = jsonNode.get("value").get("eventType").asInt();
            LogEntity logEntity = new LogEntity(userId, itemList, type, triggerTime, isRecommend, event_type);
            return logEntity;
        }
    }

    /**
     * 获取窗口截止时间
     */
    private static class WindowResultFunction implements WindowFunction<TopItemEntity, UserViewCount, String, TimeWindow> {
        @Override
        public void apply(String key, TimeWindow window, Iterable<TopItemEntity> input, Collector<UserViewCount> out) throws Exception {
            TopItemEntity count = input.iterator().next();
            out.collect(new UserViewCount(key, window.getEnd(), count.getActionTimes()));
        }
    }

    private static class TopNProds extends KeyedProcessFunction<Tuple, UserViewCount, List<String>> {

        private final String[] PROD_LIST =
                {"1666681382815881402",
                        "1666681382815881403",
                        "1666681382815881404",
                        "1666681382815881405",
                        "1666681382815881409",
                        "1666681382815881450",
                        "1666681382815881451",
                        "1666681382815881452",
                        "1666681382815881453",
                        "1666681382815881454",
                        "1666681382815881455",
                        "1666681382815881456",
                        "1666681382815881457",
                        "1666681382815881458",
                        "1666681382815881459",
                        "1666681382815881460",
                        "1666681382815881461",
                        "1666681382815881462",
                        "1666681382815881463",
                        "1666681382815881464",
                        "1666681382815881465",
                        "1666681382815881466",
                        "1666681382815881467",
                        "1666681382815891442",
                        "1666681382815891443",
                        "1666681382815891444",
                        "1666681382815891445",
                        "1666681382815891450",
                        "1666681382815891451",
                        "1666681382815891452",
                        "1666681382815891453",
                        "1666681382815891454",
                        "1666681382815891455",
                        "1666681382815891456",
                        "1666681382815891457",
                        "1666681382815891458",
                        "1666681382815891459",
                        "826720212936458240",
                        "826720898705162240",
                        "826721749876244480",
                        "826722340635574272",
                        "826722495610912768",
                        "826723024386818048",
                        "826723791994781696",
                        "826724238646214656",
                        "826724416971243520",
                        "826724768562970624",
                        "826724975539290112",
                        "826725272856723456",
                        "826725414913605632",
                        "826725597697179648",
                        "826725787246166016",
                        "826756550628900864"};
        private int topSize;
        private ListState<UserViewCount> userViewCountListState;

        public TopNProds(int topSize) {
            this.topSize = topSize;
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<List<String>> out) throws Exception {
            super.onTimer(timestamp, ctx, out);
            List<UserViewCount> userViewCounts = new ArrayList<>();
            for(UserViewCount userViewCount : userViewCountListState.get()) {
                userViewCounts.add(userViewCount);
            }

            userViewCountListState.clear();

            userViewCounts.sort(new Comparator<UserViewCount>() {
                @Override
                public int compare(UserViewCount o1, UserViewCount o2) {
                    return (int)(o2.getViewCount() - o1.getViewCount());
                }
            });
            List<String> topList = new ArrayList<String>();
            int i = 0;
            for(UserViewCount item : userViewCounts){
                i++;
                topList.add(item.getItemId());
                if(i>=topSize){
                    break;
                }
            }
            int j = 0;
            while(topList.size()<topSize){
                if(!topList.contains(PROD_LIST[j])){
                    topList.add(PROD_LIST[j]);
                }
                j++;
            }
            out.collect(topList);
        }

        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
            ListStateDescriptor<UserViewCount> userViewCountListStateDescriptor = new ListStateDescriptor<>(
                    "user-state",
                    UserViewCount.class
            );
            userViewCountListState = getRuntimeContext().getListState(userViewCountListStateDescriptor);
        }

        @Override
        public void processElement(UserViewCount value, Context ctx, Collector<List<String>> out) throws Exception {
            userViewCountListState.add(value);
            ctx.timerService().registerEventTimeTimer(value.getWindowEnd() + 1000);
        }
    }

    private static class TopNCards extends KeyedProcessFunction<Tuple, UserViewCount, List<String>> {

        private int topSize;
        private ListState<UserViewCount> userViewCountListState;

        public TopNCards(int topSize) {
            this.topSize = topSize;
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<List<String>> out) throws Exception {
            super.onTimer(timestamp, ctx, out);
            List<UserViewCount> userViewCounts = new ArrayList<>();
            for(UserViewCount userViewCount : userViewCountListState.get()) {
                userViewCounts.add(userViewCount);
            }

            userViewCountListState.clear();

            userViewCounts.sort(new Comparator<UserViewCount>() {
                @Override
                public int compare(UserViewCount o1, UserViewCount o2) {
                    return (int)(o2.getViewCount() - o1.getViewCount());
                }
            });
            List<String> topList = new ArrayList<String>();
            int i = 0;
            for(UserViewCount item : userViewCounts){
                i++;
                topList.add(item.getItemId());
                if(i>=topSize){
                    break;
                }
            }
            int j = 0;
            while(topList.size()<topSize){
                j++;
                if(!topList.contains(String.valueOf(j))){
                    topList.add(String.valueOf(j));
                }
            }

            out.collect(topList);
        }

        @Override
        public void open(org.apache.flink.configuration.Configuration parameters) throws Exception {
            super.open(parameters);
            ListStateDescriptor<UserViewCount> userViewCountListStateDescriptor = new ListStateDescriptor<>(
                    "user-state",
                    UserViewCount.class
            );
            userViewCountListState = getRuntimeContext().getListState(userViewCountListStateDescriptor);
        }

        @Override
        public void processElement(UserViewCount value, Context ctx, Collector<List<String>> out) throws Exception {
            userViewCountListState.add(value);
            ctx.timerService().registerEventTimeTimer(value.getWindowEnd() + 1000);
        }
    }
}
