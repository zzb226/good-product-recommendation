package com.demo.task;

import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;
import java.util.HashMap;
import java.util.Map;

public class ScoreAggregator extends KeyedProcessFunction<String, Tuple3<String, String, Integer>, Map<String,Map<String,Integer>>> {
    private transient MapState<String, Integer> scoreMapState;


    @Override
    public void open(Configuration parameters) throws Exception {
        // 初始化MapState
        MapStateDescriptor<String, Integer> descriptor = new MapStateDescriptor<>("scoreMapState", String.class, Integer.class);
        scoreMapState = getRuntimeContext().getMapState(descriptor);

    }

    @Override
    public void close() throws Exception {
        super.close();
        scoreMapState.clear();
    }

    @Override
    public void processElement(Tuple3<String, String, Integer> value, Context ctx, Collector<Map<String,Map<String,Integer>>> out) throws Exception {
        // 获取当前userId
        String userId = value.f0;
        // 获取当前itemId
        String itemId = value.f1;
        // 获取当前score
        int score = value.f2;

        // 更新scoreMapState
        if (scoreMapState.contains(itemId)) {
            // 如果scoreMapState中已存在itemId，将当前score+原有的score取平均
            int currentScore = scoreMapState.get(itemId);
            scoreMapState.put(itemId, (currentScore + score)/2);
        } else {
            // 如果scoreMapState中不存在itemId，将当前score作为初始值
            scoreMapState.put(itemId, score);
        }


        Map<String, Integer> itemMap = new HashMap<>();
        for (Map.Entry<String, Integer> entry : scoreMapState.entries()) {
            itemMap.put(entry.getKey(), entry.getValue());
        }
        Map<String,Map<String,Integer>> resultMap = new HashMap<>();
        resultMap.put(ctx.getCurrentKey(), itemMap);

        // 输出结果
        out.collect(resultMap);
    }


}
