package com.ebanma.cloud.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.sun.tracing.dtrace.ModuleName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author 贾清博
 * {@code @date} 2023-02-13
 * @version 1.0
 * {@code @description} 数据类
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TrackingData implements Serializable {
    /**
     * 用户id
     */
    private String userId;
    /**
     * appid
     */
    private String appId;
    /**
     * app版本
     */
    private String appVersion;
    /**
     * 事件名称
     */
    private String moduleName;
    /**
     * 事件类型
     */
    private String moduleType;
    /**
     * 事件数据
     */
    private String eventData;
    /**
     * 事件ID
     */
    private String eventId;
    /**
     * 开始时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /**
     * 触发时间
     */
    @JSONField(format = "yyyy-MM-dd")
    private Date triggerTime;
    /**
     * 结束时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    /**
     * 车架号
     */
    private String vin;
    /**
     * 汽车数据
     */
    private String carData;




    @Override
    public String toString() {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf2=new SimpleDateFormat("yyyyMMdd");
        return userId+","+
                appId+","+
                appVersion+","+
                sdf.format(endTime)+","+
                moduleName +","+
                moduleType+","+
                eventData+","+
                eventId+","+
                sdf.format(startTime)+","+
                sdf2.format(triggerTime)+","+
                vin+","+
                carData;
    }
}

