package com.ebanma.cloud.controller;

import com.ebanma.cloud.entity.DataRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/tracking")
public class TrackingClientController{

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 接受请求转发给服务提供者
     * @param request
     * @return
     */
    @PostMapping ("/client/lb")
    public ResponseEntity<String> collectTracking(@RequestBody DataRequest request) {
        String result = restTemplate.postForObject("http://tracking-service" + "/tracking/collectData" ,request, String.class);
        return ResponseEntity.ok(result);
    }

}
