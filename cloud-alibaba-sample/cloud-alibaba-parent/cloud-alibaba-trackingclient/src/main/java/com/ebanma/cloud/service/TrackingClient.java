package com.ebanma.cloud.service;

import com.ebanma.cloud.entity.TrackingData;
import com.ebanma.cloud.service.impl.TrackingFallbackService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "tracking-service",fallback = TrackingFallbackService.class)
public interface TrackingClient {

    @PostMapping("/tracking/collectData")
    public ResponseEntity<String> collectData(@RequestBody TrackingData trackingData);
}
