package com.ebanma.cloud.service.impl;

import com.ebanma.cloud.entity.TrackingData;
import com.ebanma.cloud.service.TrackingClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class TrackingFallbackService implements TrackingClient {

    /**
     * 服务降级
     * @param trackingData
     * @return
     */
    @Override
    public ResponseEntity<String> collectData(TrackingData trackingData) {
        return new ResponseEntity<String>("feign调用，异常降级方法", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
