package com.ebanma.cloud.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * HiveJDBC工具类
 */
@Component
public class HiveJDBCUtils {

    /**
     * 驱动-配置文件
     */
    private static String driver;

    /**
     * url-配置文件
     */
    private static String url;

    /**
     * 用户名-配置文件
     */
    private static String username;

    /**
     * 连接
     */
    private static  Connection conn;

    /**
     * 预编译对象
     */
    private static PreparedStatement  ps;

    /**
     * 密码-配置文件
     */
    private static String password;

    @Value("${hive.driver}")
    public void setDriver(String driver) {
        HiveJDBCUtils.driver = driver;
    }

    @Value("${hive.url}")
    public void setUrl(String url) {
        HiveJDBCUtils.url = url;
    }

    @Value("${hive.username}")
    public void setUsername(String username) {
        HiveJDBCUtils.username = username;
    }

    @Value("${hive.password}")
    public void setPassword(String password) {
        HiveJDBCUtils.password = password;
    }

    /**
     * 通过JDBC执行HSQL语句
     * @param sql
     */
    public static  void hiveJDBC(String sql){

        try {
            Class.forName(driver);
            //创建连接
            conn = DriverManager.getConnection(url,username,password);
            //预编译
            //开启动态分区，默认是false
            ps = conn.prepareStatement("set hive.exec.dynamic.partition=true");
            ps.executeUpdate();
            //开启允许所有分区都是动态的，否则必须要有静态分区才能使用
            ps = conn.prepareStatement("set hive.exec.dynamic.partition.mode=nonstrict");
            ps.executeUpdate();
            ps = conn.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(ps!=null){
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
