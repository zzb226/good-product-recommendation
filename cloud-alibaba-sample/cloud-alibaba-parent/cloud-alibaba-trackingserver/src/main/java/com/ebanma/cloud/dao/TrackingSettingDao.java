package com.ebanma.cloud.dao;

import com.ebanma.cloud.entity.TrackingSetting;
import java.util.List;

public interface TrackingSettingDao {

    int deleteByPrimaryKey(Long id);

    int insert(TrackingSetting record);

    int insertSelective(TrackingSetting record);

    TrackingSetting selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TrackingSetting record);

    int updateByPrimaryKey(TrackingSetting record);

    List<TrackingSetting> list(TrackingSetting trackingSetting);

    List<String> listModuleType();

    int logicalDeleteByTrackingId(Long id);
}