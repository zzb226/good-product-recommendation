package com.ebanma.cloud.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ebanma.cloud.dao.TrackingSettingDao;
import com.ebanma.cloud.entity.TrackingData;
import com.ebanma.cloud.service.TrackingService;
import com.ebanma.cloud.util.LoggerUtils;
import com.ebanma.cloud.util.RedisUtils;
import org.slf4j.Logger;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 数据采集发送至日志文件及Kafka服务实现类
 * @Author: Sun Yao
 */
@Service
public class TrackingServiceImpl implements TrackingService {

    /**
     * kafkaTemplate对象
     */
    @Resource
    private KafkaTemplate<String, Object> kafkaTemplate;

    /**
     * 查询Topic主题的dao对象
     */
    @Resource
    private  TrackingSettingDao trackingSettingDao;

    /**
     * redis工具类对象
     */
    @Resource
    private RedisUtils redisUtils;

    /**
     * 初始化将所有的moduleType存放至Redis中以便后续查询
     */
    @PostConstruct
    public void initModuleType(){
        //类初始化时加载所有的moduleType至Redis，只执行一次
        List<String> listModuleType = trackingSettingDao.listModuleType();
        redisUtils.setCacheList("moduleType",listModuleType);
        redisUtils.expire("moduleType",7, TimeUnit.DAYS);
    }


    /**
     * 采集数据服务层实现
     * 实现数据写入日志文件及KAFKA
     * @param trackingData 采集的数据
     */
    @Override
    public void collectData(TrackingData trackingData) {
        //从数据中获取模块类型
        String moduleType = trackingData.getModuleType();
        //判断moduleType是否在redis中存在
        List<String> moduleTypeList = redisUtils.getCacheList("moduleType");
        //如果moduleType在redis中存在
        if(moduleTypeList!=null&&moduleTypeList.contains(moduleType)){
            //将数据写入Kafka和日志文件
            saveData(trackingData);
        }else{
            //在redis中不存在，查询数据库
            List<String> listModuleType = trackingSettingDao.listModuleType();
            if(listModuleType.contains(moduleType)){
                //数据库存存在而redis中不存在
                //将数据写入Kafka和日志文件
                saveData(trackingData);
                //将数据写入Reids中
                moduleTypeList.add(moduleType);
                redisUtils.setCacheList("moduleType",moduleTypeList);
                //设置7天后过期
                redisUtils.expire("moduleType",7, TimeUnit.DAYS);
            }
        }

    }

    /**
     * 将数据存放日志文件及发送至相应主题的Kafka中
     * @param trackingData 采集的数据
     */
    private void saveData(TrackingData trackingData){
        try{
            //写日志文件
            Logger logger = LoggerUtils.getLogger(trackingData.getModuleType(), TrackingServiceImpl.class);
            logger.info(trackingData.toString());
            //写入Kafka,向moduleType主题发送trackingData消息
           kafkaTemplate.send(trackingData.getModuleType(), JSONObject.toJSONString(trackingData));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
