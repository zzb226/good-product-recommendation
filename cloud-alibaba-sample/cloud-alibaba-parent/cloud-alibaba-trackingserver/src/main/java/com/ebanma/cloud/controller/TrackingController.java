package com.ebanma.cloud.controller;

import com.alibaba.fastjson.JSONObject;
import com.ebanma.cloud.entity.*;
import com.ebanma.cloud.service.TrackingService;
import com.ebanma.cloud.util.ApiAbstract;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

@RestController
@RequestMapping("/tracking")
@RefreshScope
public class TrackingController extends ApiAbstract<DataResponse> {

    /**
     * trackingService服务层对象
     */
    @Resource
    private TrackingService trackingService;

    /**
     * 采集数据Controller层，进行请求解密
     * @param request 请求
     * @return
     */
    @PostMapping("/collectData")
    public DataResponse collectTracking(@RequestBody DataRequest request) {
        return process(request);
    }

    /**
     * 请求数据解密后执行方法
     * @param o 解密后参数
     * @return
     */
    @Override
    protected DataResponse work(JSONObject o) {
        TrackingData trackingData = o.toJavaObject(TrackingData.class);
        trackingService.collectData(trackingData);
        return null;
    }
}
