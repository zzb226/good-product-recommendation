package com.ebanma.cloud.service;

import com.ebanma.cloud.entity.TrackingData;

/**
 * 采集数据服务层接口
 * @Author: Sun Yao
 */
public interface TrackingService {


    /**
     * 采集数据服务层接口
     * @param trackingData 采集的数据
     */
    void collectData(TrackingData trackingData);
}
