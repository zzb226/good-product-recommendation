package com.ebanma.cloud.dao;


import com.ebanma.cloud.entity.RSASecurity;
import java.util.List;

public interface RSASecurityDao {
    int deleteByPrimaryKey(Long id);

    int insert(RSASecurity record);

    int insertSelective(RSASecurity record);

    RSASecurity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RSASecurity record);

    int updateByPrimaryKey(RSASecurity record);


    List<RSASecurity> list();

    String selectByAppId(String keyId);
}