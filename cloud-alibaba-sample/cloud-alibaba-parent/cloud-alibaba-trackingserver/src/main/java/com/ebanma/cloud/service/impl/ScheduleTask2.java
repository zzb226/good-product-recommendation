package com.ebanma.cloud.service.impl;

import com.ebanma.cloud.util.HDFSUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 执行定时任务，将日志文件上传至HDFS，同时将文件加载至Hive目标表中，按日期分区
 *  @Author: Sun Yao
 */
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
//@EnableScheduling   // 2.开启定时任务
public class ScheduleTask2 {

    /**
     * 日志本地路径-配置文件配置
     */
    private static String logPath;

    /**
     * HDFS存放路径-配置文件配置
     */
    private static String HDFSPath;

    /**
     * Hive数据库名-配置文件配置
     */
    private static String HiveDB;

    /**
     * HDFS连接url-配置文件配置
     */
    private static String HDFSUrl;

    /**
     * Hadoop用户名-配置文件配置
     */
    private static String HadoopUserName;

    @Value("${logger.logPath}")
    public void setLogPath(String logPath) {
        ScheduleTask2.logPath = logPath;
    }

    @Value("${hadoop.HDFSPath}")
    public void setHDFSPath(String HDFSPath) {
        ScheduleTask2.HDFSPath = HDFSPath;
    }

    @Value("${hadoop.HiveDB}")
    public void setHiveDB(String hiveDB) {
        HiveDB = hiveDB;
    }

    @Value("${hadoop.HDFSUrl}")
    public void setHDFSUrl(String HDFSUrl) {
        ScheduleTask2.HDFSUrl = HDFSUrl;
    }
    @Value("${hadoop.HadoopUserName}")
    public void setHadoopUserName(String hadoopUserName) {
        HadoopUserName = hadoopUserName;
    }

    /**
     * 添加定时任务 每天0点执行一次
     * 将日志文件上传至HDFS，同时将文件加载至Hive目标表中，按日期分区
     */
    @Scheduled(cron = "0 00 0 * * ?")
    public void uploadToHive()  {
        //获取日期，由于0点执行上传昨天的数据，所以获取时间-12h
        Date date=new Date(System.currentTimeMillis() - 1000 * 60 * 60 * 12);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
        String ModuleType ="";
        try {
        //查询目录下的所有文件
        File file =new File(logPath);
        if(file.exists()){
            if(file.isDirectory()){
                for(File f : file.listFiles()){
                    //通过文件名截取ModuleType，作为表名
                   ModuleType =f.getName().substring(0, f.getName().indexOf("-"));
                   //通过文件名截取日期作为分区依据及判断是否为本日数据
                    String flieDate = f.getName().substring(f.getName().indexOf("-") + 1, f.getName().lastIndexOf("."));
                    if(flieDate.equals(sdf.format(date))){
                        //为当天的文件
                        //发送至HDFS
                        sendFile2HDFS(logPath+f.getName(),HDFSPath+"/"+f.getName());
                        //上传至Hive
                        sendHDFS2Hive(f,ModuleType,flieDate);
                    }
                }
            }
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 将本地日志文件上传至HDFS
     * @param localPath 本地文件路径
     * @param HDFSPath HDFS路径
     */
    public void sendFile2HDFS(String localPath,String HDFSPath){
        org.apache.hadoop.conf.Configuration conf=new org.apache.hadoop.conf.Configuration();
        FileSystem fs= null;
        try {
            //判断文件是否存在（避免覆盖原数据造成丢数据）
            if(HDFSUtils.fileExist(conf, HDFSPath)){
                //如果文件存在则追加
                HDFSUtils.appendToFile(conf,localPath,HDFSPath);
            }else{
                //文件不存在则移动本地文件至hdfs
                fs = FileSystem.get(new URI(HDFSUrl),conf,HadoopUserName);
                fs.copyFromLocalFile(true,new Path(localPath),new Path(HDFSPath));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(fs!=null){
                // 关闭
                try {
                    fs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 使用SparkSQL将HDFS上的文件加载至Hive目标表内，并按照日期分区
     * @param f
     * @param ModuleType
     * @param flieDate
     */
    private void sendHDFS2Hive(File f,String ModuleType,String flieDate){
        //初始化Spark容器
        SparkConf sc = new SparkConf().setAppName("SparkSQL");
        SparkSession spark = SparkSession.builder().enableHiveSupport().config(sc).getOrCreate();
        //spark动态参数设置
        SparkSession.builder()
                //开启动态分区，默认是false
                .config("hive.exec.dynamic.partition", "true")
                //开启允许所有分区都是动态的，否则必须要有静态分区才能使用
                .config("hive.exec.dynamic.partition.mode", "nonstrict");
        //如果表不存在，创建表
        String createTable ="create table if not exists "+HiveDB+"."+ModuleType+"(\n" +
                "  userId string,\n" +
                "  appId string,\n" +
                "  appVersion string,\n" +
                "  moduleName string,\n" +
                "  moduleType string,\n" +
                "  eventData string,\n" +
                "  eventId string,\n" +
                "  startTime string,\n" +
                "  endTime string,\n" +
                "  vin string,\n" +
                "  carData string\n" +
                ")\n" +
                "partitioned by (triggerTime string)\n"+
                "row format delimited fields terminated by ',';";
        spark.sql(createTable);
        //设置分区动态，以避免不同分区写入覆盖
        spark.conf().set("spark.sql.sources.partitionOverwriteMode", "dynamic");
        //加载数据至分区表内
        String sql="load data inpath '"+HDFSPath+"/"+f.getName()+"' into table "+HiveDB+"."+ModuleType+" partition(triggerTime='"+flieDate+"')";
        spark.sql(sql);
    }
}
