package com.ebanma.cloud.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import java.io.*;

/**
 * HDFS工具类
 * 支持:判断文件是否存在,追加文件内容
 */
public class HDFSUtils {

    /**
     * 判断文件是否存在
     */
    public static boolean fileExist(Configuration conf, String path) {
        FileSystem fs = null;
        try {
            fs = FileSystem.get(conf);
            return fs.exists(new Path(path));
        } catch (IOException e) {
           e.printStackTrace();
           return true;
        }
    }

    /**
     * 追加文件内容
     */
    public static void appendToFile(Configuration conf, String localFilePath, String remoteFilePath) {
        FileSystem fs = null;
        FileInputStream in =null;
        FSDataOutputStream out=null;
        try {
            fs = FileSystem.get(conf);
            Path remotePath = new Path(remoteFilePath);
            /* 创建一个文件读入流 */
             in = new FileInputStream(localFilePath);
            /* 创建一个文件输出流，输出的内容将追加到文件末尾 */
            out = fs.append(remotePath);
            /* 读写文件内容 */
            byte[] data = new byte[1024];
            int read = -1;
            while ( (read = in.read(data)) > 0 ) {
                out.write(data, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(fs!=null){
                try {
                    fs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(out!=null){
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(in!=null){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
