package com.ebanma.cloud.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ebanma.cloud.dao.RSASecurityDao;
import com.ebanma.cloud.entity.DataRequest;
import com.ebanma.cloud.entity.DataResponse;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 请求数据解密
 * @param <R>
 */
@Slf4j
public abstract class ApiAbstract<R> {

    /**
     * 密钥查询dao层对象
     */
    @Resource
    private RSASecurityDao rsaSecurityDao;

    /**
     * redis工具类对象
     */
    @Resource
    private RedisUtils redisUtils;


    /**
     * 统一处理
     *
     * @param dataRequest 统一请求
     * @return 统一响应
     */
    public DataResponse process(DataRequest dataRequest) {
        try {
            String privateKey = getPrivateKey(dataRequest.getKeyId());
            JSONObject o = before(dataRequest.getData(), privateKey);
            R r = work(o);
            return after(r, privateKey);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("ServerAbstract process exception! dataRequest={}", dataRequest, e);
            return DataResponse.builder()
                    .success(false)
                    .message("系统异常")
                    .build();
        }

    }

    /**
     * 处理前置解密
     *
     * @param data       请求密文
     * @param privateKey 秘钥
     * @return 请求参数
     */
    private JSONObject before(String data, String privateKey) {
        return JSON.parseObject(RsaUtil.decryptPrivate(privateKey, data));
    }

    /**
     * 业务处理
     *
     * @param o 入参
     * @return 返回
     */
    protected abstract R work(JSONObject o);

    /**
     * 后置处理加密
     *
     * @param r          业务数据
     * @param privateKey 秘钥
     * @return 统一返回
     */
    private DataResponse after(R r, String privateKey) {
        return DataResponse.builder()
                .success(true)
                .message("交易成功")
                .data(RsaUtil.encryptPrivate(privateKey, JSON.toJSONString(r)))
                .build();
    }

    /**
     * 通过appId获取对应的私钥，不同的接入方提供不同的公私钥。
     * 实际业务开发中这些会存在文件中或者配置中心中如阿波罗，这里简单实现
     *
     * @param keyId keyId
     * @return 私钥
     */
    private String getPrivateKey(String keyId) {
        String privateKey =null;
        //判断是否在redis中存在
       privateKey = redisUtils.getCacheObject(keyId);
        if (privateKey != null) {
            //存在
            return privateKey;
        } else {
            //redis中不存在，去数据库查询
            privateKey = rsaSecurityDao.selectByAppId(keyId);
            if (privateKey != null) {
                //存在 加入redis
                redisUtils.setCacheObject(keyId, privateKey);
                redisUtils.expire(keyId, 7, TimeUnit.DAYS);
            }
        }
        return privateKey;
    }
}

